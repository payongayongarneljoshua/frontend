
import React, {useState } from 'react';
import { useEffect } from 'react/cjs/react.development';
import './Calculator.css';
import CalculatorButton from './CalculatorButton.js'
import CalculatorInput  from './CalculatorInput';
export const InputContext = React.createContext('');

function Calculator() {

    const operators = {
        '+' : (num1,num2) => {
          return num1 + num2
        },
        '-' : (num1,num2) => {
          return num1 - num2
        },
        '÷' : (num1,num2) => {
          return num1 / num2
        },
        'X' : (num1,num2) => {
          return num1 * num2
        },
    }

    const clear = () => {
        setIsOperator(false)
        setOperator('')
        setInput({name: '0',type: 'key'})
        setPreviousInput('')
    }

    const percentage = () => {
        if (parseFloat(input.name) === 0) return
      
        const newValue = parseFloat(input.name) / 100

        setInput({
            name: newValue,
            type: 'key'
        })
    }

    const addDecimal = () => {
        let newValue = (!isDecimal) ? `${input.name}.`: input.name

        setInput({
            name: newValue,
            type: 'key'
        })
        setIsDecimal(true)
    }

    const negate = () => {
        let value = parseFloat(input.name)
        let newValue = ''
        if(value < 0){
          newValue = parseFloat(input.name) * -1
        }else{
          newValue = `-${input.name}`
        } 

        setInput({
          name: newValue.toString(),
          type: 'key'
        })
    }

    const equal = () => {
        if(isOperator){
            let previous = parseFloat(previousInput.name)
            let current = parseFloat(input.name)

            let answer = operators[operator](previous,current)
    
            setInput({
              name: answer,
              type: 'key'
            })

            setIsOperator(false)
            setIsDecimal(false)
            setOperator('')
        }
    }
  
    const [previousInput,setPreviousInput] = useState('')
    const [input,setInput] = useState({name: '0',type: 'key'})
    const [operator,setOperator] = useState('')
    const [isOperator,setIsOperator] = useState(false)
    const [isDecimal,setIsDecimal] = useState(false)
    const [formattedValue,setFormattedValue] = useState('')
    const [responsive,setResponsive] = useState()


    const getInput = (inputted) => {
     

      if(inputted.type == 'operator'){
        if(input == '') return

        switch(inputted.name){
            case 'C':
                clear()
                break;
            case '%':
                percentage()
                break;
            case '.':
                addDecimal()
                break;
            case '±':
                negate()
                break;
            case '=':
                equal()
                break;
            default:
                setIsOperator(true)
                setOperator(inputted.name)
                setPreviousInput(input)
                setInput('0')
                break;

        }
        
      }else{  
        inputted.name = ((input.name !== undefined)? input.name : '') + inputted.name
        setInput(inputted);
      }
    }
    


    useEffect( () => {
        
      let value = parseFloat(input.name)


      if(!isNaN(value)){
          const language = navigator.language || 'en-US'
          let value = ''
          let formattedValue = parseFloat(input.name).toLocaleString(language, {
          useGrouping: true,
          maximumFractionDigits: 6
          })
          
          const match = value.match(/\.\d*?(0*)$/)
          
          if (match) formattedValue += (/[1-9]/).test(match[0]) ? match[1] : match[0]


          let length = 15 / input.name.length
          
          let responsive = null

          if(input.name == '0'){
              responsive = {
                  fontSize:  `5rem`
              }

              setResponsive(responsive)
          }


          if(input.name.length % 7 == 0 && length > 2){
          
              responsive = {
                  fontSize: `${(input.name.length / 5)}rem`
              }
                 
              setResponsive(responsive)
          }
         

          setFormattedValue(formattedValue)
      }
    },[input])
    return (
      <>
       <InputContext.Provider value={formattedValue} >
          <div className="calculator">
            <div className="calculator-input" >
              <CalculatorInput responsive={responsive}/>
            </div>
  
            <div className='calculator-body'>
              <div className="d-grid d-grid-4-auto bg-black">
                  <InputContext.Provider value={input}>
                    <CalculatorButton  handleInput={getInput} name="C" type="operator"/>
                    <CalculatorButton  handleInput={getInput} name="±" type="operator"/>
                    <CalculatorButton  handleInput={getInput} name="%" type="operator"/>
                    <CalculatorButton  handleInput={getInput} name="÷" type="operator"/>
                  </InputContext.Provider>
              </div>
                
              <div className='d-grid d-grid-4-auto bg-gray'>
                <InputContext.Provider value={input}>
                  <CalculatorButton  handleInput={getInput} name="7" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="8" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="9" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="X" type="operator"/>
                </InputContext.Provider>
              </div>
  
              <div className='d-grid d-grid-4-auto bg-gray-light'>
                <InputContext.Provider value={input}>
                  <CalculatorButton  handleInput={getInput} name="4" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="5" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="6" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="-" type="operator"/>
                </InputContext.Provider>
              </div>
  
              <div className='d-grid d-grid-4-auto bg-gray-light'>
                <InputContext.Provider value={input}>
                  <CalculatorButton  handleInput={getInput} name="1" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="2" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="3" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="+" type="operator"/>
                </InputContext.Provider>
              </div>
  
              <div className='d-grid d-grid-3-auto bg-gray-light'>
                <InputContext.Provider value={input}>
                  <CalculatorButton  handleInput={getInput} name="0" type="key"/>
                  <CalculatorButton  handleInput={getInput} name="." type="operator"/>
                  <CalculatorButton  handleInput={getInput} name="=" type="operator"/>
                </InputContext.Provider>
              </div>
            </div>
          </div>
        </InputContext.Provider>
      </>
    )
  }
  
  export default Calculator;
  