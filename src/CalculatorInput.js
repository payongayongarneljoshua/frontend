import React, { useContext,useEffect } from 'react';

import {InputContext} from './Calculator'



function CalculatorInput(props){


    const input  = useContext(InputContext)
    const responsive = props.responsive


    return (
        <h4 style={responsive}>{input}</h4>
    )
}

export default CalculatorInput