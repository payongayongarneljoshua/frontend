import './CalculatorButton.css'

import {useCallback } from 'react';


function CalculatorButton(props){

    const handleClick = useCallback(() => {
        props.handleInput({
            name: props.name,
            type: props.type

        })
      }, [props]);

    return (
        <button className={props.type} onClick={handleClick}>{props.name}</button>
    )
}


export default CalculatorButton